
// 1. Створення змінної в JavaScript називається "оголошенням" змінної.
// Оголосити змінну в Java Script можна за допомогою, ключового слова:
// var, let або const, імені, оператора =, та занчення змінної.



// 2. Функція prompt  показує вікно з текстовим повідомленням, полем,
// куди відвідувач може ввести текст, та кнопками ОК/Скасувати.
// Функція confirm показує вікно з питанням та 
// двома кнопками: ОК та Скасувати.
// true, якщо натиснути кнопку OK, по-другому — false.




// 3. Неявне перетворення типів- це автоматичне перетворення типів при
// компіляції
// Приклад: '123' + 5
//            1235
//            + конкатинує рядки





// ---------------------------------------


let userName = 'Kirill';
let admin = (`${userName}`);

console.log(`${admin}`);




// ----------------------------


const days = (5);
console.log(days * 60);


// ---------------------------------


const thirdUserName = prompt('Enter your name', 'Name');

console.log(thirdUserName);

alert(`Hello, user with name ${thirdUserName}`)


